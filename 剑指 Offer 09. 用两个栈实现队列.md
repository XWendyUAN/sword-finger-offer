## 剑指 Offer 09. 用两个栈实现队列

用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead，分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead 操作返回 -1 )

https://leetcode.cn/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof/description/?envType=study-plan&id=lcof&plan=lcof&plan_progress=fyvjvg1

### 题解

实在是很久不写，只记得概念了 
设置两个栈，一个放入，另外一个做倒置（由第一个栈push进第二个） 
appendTail()元素入栈加入队尾巴 
deleteHead()删除队首，首先判断后面的栈是否为空，否则由第一个栈push进第二个，两个都空返回-1 

### c++代码

```c++
class CQueue {
public:
    stack<int> instack,outstack;
    void turn(){
        while(!instack.empty()){
            outstack.push(instack.top());
            instack.pop();
        }
	}

	CQueue() {

	}

	void appendTail(int value) {
    	instack.push(value);
	}

    int deleteHead() {
        if(outstack.empty()){
            if(instack.empty())
                return -1;
            turn();
        }
        int value = outstack.top();
        outstack.pop();
        return value;

    }
};

/**

 * Your CQueue object will be instantiated and called as such:
 * CQueue* obj = new CQueue();
 * obj->appendTail(value);
 * int param_2 = obj->deleteHead();
   */
```