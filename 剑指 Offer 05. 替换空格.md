### 剑指 Offer 05. 替换空格

请实现一个函数，把字符串 `s` 中的每个空格替换成"%20"。

https://leetcode.cn/problems/ti-huan-kong-ge-lcof/description/?envType=study-plan&id=lcof&plan=lcof&plan_progress=fyvjvg1

#### 题解    

郁闷，第一次用s.insert()显示超时   

后来遍历string好像不能用s[i++]，总是显示越界   

最后还是用了push_back()，好歹不报错了   

#### c++代码   

```c++
class Solution {
public:
    string replaceSpace(string s) {
        string s1;
        for(int c = 0; c<s.size();c++){
            if(s[c]==' '){
                s1.push_back('%');
                s1.push_back('2');
                s1.push_back('0');
            }else{
                s1.push_back(s[c]);
            }
        }
        return s1;
    }
};
```

