#### 剑指 Offer 30. 包含min函数的栈

定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。   

https://leetcode.cn/problems/bao-han-minhan-shu-de-zhan-lcof/description/?envType=study-plan&id=lcof&plan=lcof&plan_progress=fyvjvg1   

##### 题解   

使用两个栈，一个按序存放，一个永远放比较后较小数，保证栈顶是最小数

两个栈同时存入取出

##### c++代码   

```c++
class MinStack {
public:
    /** initialize your data structure here. */
    stack<int> s;
    stack<int> mins;

    MinStack() {
        mins.push(INT_MAX);
    }
    
    void push(int x) {
        s.push(x);
        mins.push(::min(mins.top(),x));
    }
    
    void pop() {
        s.pop();
        mins.pop();
    }
    
    int top() {
        return s.top();
    }
    
    int min() {
        return mins.top();
    }
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(x);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->min();
 */
```

