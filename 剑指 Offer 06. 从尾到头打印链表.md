### 剑指 Offer 06. 从尾到头打印链表

输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。

https://leetcode.cn/problems/cong-wei-dao-tou-da-yin-lian-biao-lcof/description/?envType=study-plan&id=lcof&plan=lcof&plan_progress=fyvjvg1

#### 题解    

最近和栈杠上了，从头到尾马上想到栈，依次压入栈，然后新建一个数组返回

#### c++代码   

```c++
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> reversePrint(ListNode* head) {
        stack<int> s;
        ListNode* t = head;
        while (t != NULL) {
            s.push(t->val);
            t = t->next;
        } 
        //新建数组返回
        vector<int> a;
        while (!s.empty()) {
            a.push_back(s.top());
            s.pop();
        }
        return a;
    }
};
```

